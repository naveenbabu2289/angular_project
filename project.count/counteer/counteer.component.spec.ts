import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CounteerComponent } from './counteer.component';

describe('CounteerComponent', () => {
  let component: CounteerComponent;
  let fixture: ComponentFixture<CounteerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounteerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounteerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
