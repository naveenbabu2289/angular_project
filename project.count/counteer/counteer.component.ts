import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-counteer',
  templateUrl: './counteer.component.html',
  styleUrls: ['./counteer.component.css']
})
export class CounteerComponent {
  @Input() count: number = 0;
  increment() {
    this.count++;
  }
  decrement() {
    this.count--;
  }
}
