import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ToDoService } from './todo.service';
import { TodoComponent } from './todo/todo.component';
import { TodolistComponent } from './todolist/todolist.component';
import { TodosComponent } from './todos/todos.component';
import { TodoFormComponent } from './todo-form/todo-form.component';

@NgModule({
  declarations: [
    AppComponent,
    TodoComponent,
    TodolistComponent,
    TodosComponent,
    TodoFormComponent
  ],
  imports: [BrowserModule, FormsModule],
  providers: [ToDoService],
  bootstrap: [AppComponent]
})
export class AppModule {}
