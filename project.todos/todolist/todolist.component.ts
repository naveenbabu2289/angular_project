import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ToDo } from '../todo';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css']
})
export class TodolistComponent {
  @Input() todos: ToDo[];

  @Output() onComplete = new EventEmitter();

  @Output() onDelete = new EventEmitter();

  constructor() {}
}
