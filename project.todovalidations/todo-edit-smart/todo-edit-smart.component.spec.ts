import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoEditSmartComponent } from './todo-edit-smart.component';

describe('TodoEditSmartComponent', () => {
  let component: TodoEditSmartComponent;
  let fixture: ComponentFixture<TodoEditSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodoEditSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoEditSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
