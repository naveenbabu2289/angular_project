export interface ToDo {
  label: string;
  id: number;
  complete: boolean;
}
