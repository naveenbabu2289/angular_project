import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ToDo } from '../todo';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css']
})
export class TodolistComponent {
  @Input() todos: ToDo[];

  @Output() OnComplete = new EventEmitter();

  @Output() OnDelete = new EventEmitter();

  constructor() {}
}
