import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { ToDo } from '../todo';

@Component({
  selector: 'app-todo-edit',
  templateUrl: './todo-edit.component.html',
  styleUrls: ['./todo-edit.component.css']
})
export class TodoEditComponent implements OnInit {
  @Input() todo: ToDo;

  @Output() OnUpdate = new EventEmitter();

  constructor() {}

  update() {
    this.OnUpdate.emit(this.todo);
  }

  ngOnInit() {}
}
